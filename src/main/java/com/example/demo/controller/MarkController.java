package com.example.demo.controller;

import com.example.demo.dto.MarkDto;
import com.example.demo.dto.form.MarkForm;
import com.example.demo.service.impl.MarkServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/mark")
@RequiredArgsConstructor
public class MarkController {

    private final MarkServiceImpl service;

    @GetMapping("/{id}")
    public ResponseEntity<MarkDto> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @GetMapping
    public ResponseEntity<List<MarkDto>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping
    public ResponseEntity<MarkDto> add(@RequestBody MarkForm form) {
        return ResponseEntity.ok(service.add(form));
    }

    @PutMapping("/{id}")
    public ResponseEntity<MarkDto> update(@PathVariable Long id, @RequestBody MarkForm form) {
        return ResponseEntity.ok(service.update(id, form));
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
