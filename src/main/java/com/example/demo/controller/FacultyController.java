package com.example.demo.controller;

import com.example.demo.dto.FacultyDto;
import com.example.demo.dto.StudentResponse;
import com.example.demo.dto.form.FacultyForm;
import com.example.demo.service.impl.FacultyServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/faculty")
@RequiredArgsConstructor
public class FacultyController {

    private final FacultyServiceImpl service;

    @GetMapping("/{id}")
    public ResponseEntity<FacultyDto> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @GetMapping
    public ResponseEntity<List<FacultyDto>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping
    public ResponseEntity<FacultyDto> add(@RequestBody FacultyForm form) {
        return ResponseEntity.ok(service.add(form));
    }

    @PutMapping("/{id}")
    public ResponseEntity<FacultyDto> update(@PathVariable Long id, @RequestBody FacultyForm form) {
        return ResponseEntity.ok(service.update(id, form));
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }

    @GetMapping("/university/{universityId}")
    public ResponseEntity<List<FacultyDto>> findByUniversity(@PathVariable Long universityId) {
        return ResponseEntity.ok(service.findByUniversity(universityId));
    }

    @GetMapping("/university/count/{universityId}")
    public ResponseEntity<Integer> getCountByUniversity(@PathVariable Long universityId) {
        return ResponseEntity.ok(service.getCountByUniversity(universityId));
    }

    @GetMapping("/list/{id}")
    public ResponseEntity<List<StudentResponse>> studentList(@PathVariable Long id){
        return ResponseEntity.ok(service.studentList(id));
    }
}
