package com.example.demo.controller;

import com.example.demo.dto.GroupDto;
import com.example.demo.dto.StudentDto;
import com.example.demo.dto.StudentResponse;
import com.example.demo.dto.form.GroupForm;
import com.example.demo.service.impl.GroupServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/group")
@RequiredArgsConstructor
public class GroupController {

    private final GroupServiceImpl service;

    @GetMapping("/{id}")
    public ResponseEntity<GroupDto> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @GetMapping
    public ResponseEntity<List<GroupDto>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping
    public ResponseEntity<GroupDto> add(@RequestBody GroupForm form) {
        return ResponseEntity.ok(service.add(form));
    }

    @PutMapping("/{id}")
    public ResponseEntity<GroupDto> update(@PathVariable Long id, @RequestBody GroupForm form) {
        return ResponseEntity.ok(service.update(id, form));
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }

    @GetMapping("/faculty/{facultyId}")
    public ResponseEntity<List<GroupDto>> findByFaculty(@PathVariable Long facultyId) {
        return ResponseEntity.ok(service.findByFaculty(facultyId));
    }

    @GetMapping("/faculty/count/{facultyId}")
    public ResponseEntity<Integer> getCountByFaculty(@PathVariable Long facultyId) {
        return ResponseEntity.ok(service.getCountByFaculty(facultyId));
    }

    @GetMapping("/university/{universityId}")
    public ResponseEntity<List<GroupDto>> findByUniversity(@PathVariable Long universityId) {
        return ResponseEntity.ok(service.findByUniversity(universityId));
    }

    @GetMapping("/university/count/{universityId}")
    public ResponseEntity<Integer> getCountByUniversity(@PathVariable Long universityId) {
        return ResponseEntity.ok(service.getCountByUniversity(universityId));
    }

    @GetMapping("/list/{id}")
    public ResponseEntity<List<StudentResponse>> studentList(@PathVariable Long id) {
        return ResponseEntity.ok(service.studentList(id));
    }
}
