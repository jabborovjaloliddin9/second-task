package com.example.demo.controller;

import com.example.demo.dto.StudentDto;
import com.example.demo.dto.StudentNameResponse;
import com.example.demo.dto.form.StudentForm;
import com.example.demo.service.impl.StudentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentServiceImpl service;

    @GetMapping("/{id}")
    public ResponseEntity<StudentDto> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @GetMapping
    public ResponseEntity<List<StudentDto>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping
    public ResponseEntity<StudentDto> add(@RequestBody StudentForm form) {
        return ResponseEntity.ok(service.add(form));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StudentDto> update(@PathVariable Long id, @RequestBody StudentForm form) {
        return ResponseEntity.ok(service.update(id, form));
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id){
        service.delete(id);
    }

    @GetMapping("/firstname")
    public ResponseEntity<List<StudentNameResponse>> getByFirstName(@RequestParam String firstname){
        return ResponseEntity.ok(service.getByFirstName(firstname));
    }

    @GetMapping("/lastname")
    public ResponseEntity<List<StudentNameResponse>> getByLastName(@RequestParam String lastname){
        return ResponseEntity.ok(service.getByLastName(lastname));
    }

    @GetMapping("/group/{groupId}")
    public ResponseEntity<List<StudentDto>> findByGroup(@PathVariable Long groupId){
        return ResponseEntity.ok(service.findByGroup(groupId));
    }

    @GetMapping("/group/count/{groupId}")
    public ResponseEntity<Integer> getCountByGroup(@PathVariable Long groupId){
        return ResponseEntity.ok(service.getCountByGroup(groupId));
    }

    @GetMapping("/faculty/{facultyId}")
    public ResponseEntity<List<StudentDto>> findByFaculty(@PathVariable Long facultyId){
        return ResponseEntity.ok(service.findByFaculty(facultyId));
    }

    @GetMapping("/faculty/count/{facultyId}")
    public ResponseEntity<Integer> getCountByFaculty(@PathVariable Long facultyId){
        return ResponseEntity.ok(service.getCountByFaculty(facultyId));
    }

    @GetMapping("/university/{universityId}")
    public ResponseEntity<List<StudentDto>> findByUniversity(@PathVariable Long universityId){
        return ResponseEntity.ok(service.findByUniversity(universityId));
    }

    @GetMapping("/university/count/{universityId}")
    public ResponseEntity<Integer> getCountByUniversity(@PathVariable Long universityId){
        return ResponseEntity.ok(service.getCountByUniversity(universityId));
    }
}
