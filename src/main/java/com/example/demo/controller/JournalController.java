package com.example.demo.controller;

import com.example.demo.dto.JournalDto;
import com.example.demo.dto.StudentDto;
import com.example.demo.dto.SubjectDto;
import com.example.demo.dto.form.JournalForm;
import com.example.demo.dto.form.JournalRequest;
import com.example.demo.service.impl.JournalServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/journal")
@RequiredArgsConstructor
public class JournalController {

    private final JournalServiceImpl service;

    @GetMapping("/{id}")
    public ResponseEntity<JournalDto> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @GetMapping
    public ResponseEntity<List<JournalDto>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping
    public ResponseEntity<JournalDto> add(@RequestBody JournalForm form) {
        return ResponseEntity.ok(service.add(form));
    }

    @PutMapping("/{id}")
    public ResponseEntity<JournalDto> update(@PathVariable Long id, @RequestBody JournalForm form) {
        return ResponseEntity.ok(service.update(id, form));
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }

    @PutMapping("/subject/add")
    void addSubjects(@RequestBody JournalRequest request) {
        service.addSubjects(request);
    }

    @PutMapping("/subject/remove")
    void removeSubjects(@RequestBody JournalRequest request) {
        service.removeSubjects(request);
    }

    @GetMapping("/student/{id}")
    public ResponseEntity<List<StudentDto>> findAllStudents(@PathVariable Long id){
        return ResponseEntity.ok(service.findAllStudents(id));
    }

    @GetMapping("/student/count/{id}")
    public ResponseEntity<Integer> getStudentCount(@PathVariable Long id){
        return ResponseEntity.ok(service.getStudentCount(id));
    }

}
