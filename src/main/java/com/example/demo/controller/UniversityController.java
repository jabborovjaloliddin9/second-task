package com.example.demo.controller;

import com.example.demo.dto.StudentResponse;
import com.example.demo.dto.UniversityDto;
import com.example.demo.dto.form.UniversityForm;
import com.example.demo.service.impl.UniversityServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/university")
@RequiredArgsConstructor
public class UniversityController {

    private final UniversityServiceImpl service;

    @GetMapping("/{id}")
    public ResponseEntity<UniversityDto> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @GetMapping
    public ResponseEntity<List<UniversityDto>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping
    public ResponseEntity<UniversityDto> add(@RequestBody UniversityForm form) {
        return ResponseEntity.ok(service.add(form));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UniversityDto> update(@PathVariable Long id, @RequestBody UniversityForm form) {
        return ResponseEntity.ok(service.update(id, form));
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id){
        service.delete(id);
    }

    @GetMapping("/list/{id}")
    public ResponseEntity<List<StudentResponse>> studentList(@PathVariable Long id){
        return ResponseEntity.ok(service.studentList(id));
    }
}
