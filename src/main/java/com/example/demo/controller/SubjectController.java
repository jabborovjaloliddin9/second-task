package com.example.demo.controller;

import com.example.demo.dto.SubjectDto;
import com.example.demo.dto.form.SubjectForm;
import com.example.demo.service.impl.SubjectServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/subject")
@RequiredArgsConstructor
public class SubjectController {

    private final SubjectServiceImpl service;

    @GetMapping("/{id}")
    public ResponseEntity<SubjectDto> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @GetMapping
    public ResponseEntity<List<SubjectDto>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping
    public ResponseEntity<SubjectDto> add(@RequestBody SubjectForm form) {
        return ResponseEntity.ok(service.add(form));
    }

    @PutMapping("/{id}")
    public ResponseEntity<SubjectDto> update(@PathVariable Long id, @RequestBody SubjectForm form) {
        return ResponseEntity.ok(service.update(id, form));
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id){
        service.delete(id);
    }

    @GetMapping("/student/{studentId}")
    public ResponseEntity<List<SubjectDto>> findByStudent(@PathVariable Long studentId){
        return ResponseEntity.ok(service.findByStudent(studentId));
    }
}
