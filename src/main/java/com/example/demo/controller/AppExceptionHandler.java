package com.example.demo.controller;

import com.example.demo.dto.ErrorResponse;
import com.example.demo.exception.InvalidMarkException;
import com.example.demo.exception.ObjectAllreadyExistsException;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.exception.UniversityException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@RequiredArgsConstructor
public class AppExceptionHandler {

    private final MessageSource source;

    @ExceptionHandler(UniversityException.class)
    public ResponseEntity<ErrorResponse> handleException(UniversityException ex) {

        if (ex instanceof ObjectNotFoundException) {
            ObjectNotFoundException exception = (ObjectNotFoundException) ex;
            return exception.response(source, exception.getObjectName(), exception.getId());
        }

        if (ex instanceof ObjectAllreadyExistsException) {
            ObjectAllreadyExistsException exception = (ObjectAllreadyExistsException) ex;
            return exception.response(source, exception.getObjectName());
        }

        if (ex instanceof InvalidMarkException) {
            InvalidMarkException exception = (InvalidMarkException) ex;
            return exception.response(source, exception.getObjectName());
        }

        return ResponseEntity.badRequest().body(ErrorResponse.of(
                111,
                ex.getMessage())
        );
    }
}