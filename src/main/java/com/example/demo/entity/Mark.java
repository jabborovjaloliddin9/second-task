package com.example.demo.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Mark extends BaseEntity {
    private Integer studentMark;
    @ManyToOne
    private Student student;
    @ManyToOne
    private Journal journal;
    @ManyToOne
    private Subject subject;
}
