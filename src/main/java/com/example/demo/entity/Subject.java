package com.example.demo.entity;

import lombok.*;

import javax.persistence.Entity;

@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Subject extends BaseEntity {
    private LocalizadString name;
}
