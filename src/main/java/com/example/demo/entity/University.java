package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class University extends BaseEntity {
    @Embedded
    private LocalizadString name;
    private String address;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate open_year;
}
