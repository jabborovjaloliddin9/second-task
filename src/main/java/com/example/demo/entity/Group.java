package com.example.demo.entity;

import com.fasterxml.jackson.databind.ser.Serializers;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@Entity(name = "groups")
@NoArgsConstructor
@AllArgsConstructor
public class Group extends BaseEntity {
    @Column(length = 128)
    private String name;
    @ManyToOne
    private Faculty faculty;
    @Column(length = 64)
    private String year;
}
