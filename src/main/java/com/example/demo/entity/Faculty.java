package com.example.demo.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Faculty extends BaseEntity {
    private LocalizadString name;
    @ManyToOne
    private University university;
}
