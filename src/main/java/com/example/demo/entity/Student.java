package com.example.demo.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Student extends BaseEntity {
    @Column(length = 128)
    private String firstName;
    @Column(length = 128)
    private String lastName;
    @ManyToOne
    private Group group;
}
