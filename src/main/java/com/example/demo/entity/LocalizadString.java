package com.example.demo.entity;

import lombok.*;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.persistence.Embeddable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class LocalizadString {
    private String uz;
    private String ru;
    private String en;

    public String locale() {
        switch (LocaleContextHolder.getLocale().toString()) {
            case "ru":
                return ru;
            case "en":
                return en;
            default:
                return uz;
        }
    }

}
