package com.example.demo.dto;

import com.example.demo.entity.University;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UniversityDto {
    private Long id;
    private String name;
    private String address;
    private LocalDate open_year;

    public static UniversityDto toDto(University university){
        return UniversityDto.builder()
                .id(university.getId())
                .name(university.getName().locale())
                .address(university.getAddress())
                .open_year(university.getOpen_year())
                .build();
    }
}
