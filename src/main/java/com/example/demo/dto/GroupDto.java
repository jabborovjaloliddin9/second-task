package com.example.demo.dto;

import com.example.demo.entity.Group;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupDto {
    private Long id;
    private String name;
    private Long facultyId;
    private String year;

    public static GroupDto toDto(Group group) {
        return GroupDto.builder()
                .id(group.getId())
                .name(group.getName())
                .facultyId(group.getFaculty().getId())
                .year(group.getYear())
                .build();
    }
}
