package com.example.demo.dto;

import com.example.demo.entity.Mark;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MarkDto {
    private Long id;
    private Integer studentMark;
    private Long studentId;
    private Long journalId;
    private Long subjectId;

    public static MarkDto toDto(Mark mark) {
        return MarkDto.builder()
                .id(mark.getId())
                .studentMark(mark.getStudentMark())
                .studentId(mark.getStudent().getId())
                .journalId(mark.getJournal().getId())
                .subjectId(mark.getSubject().getId())
                .build();
    }
}
