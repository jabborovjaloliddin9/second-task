package com.example.demo.dto;

public interface StudentResponse {
    Long getId();

    String getFirstName();

    String getLastName();

    Double getAverageMark();
}
