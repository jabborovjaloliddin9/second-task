package com.example.demo.dto;

public interface StudentNameResponse {

    Long getId();

    String getFirstName();

    String getLastName();

    Long getGroupId();

    Long getFacultyId();

    Long getUniversityId();

    Double getAverageMark();
}
