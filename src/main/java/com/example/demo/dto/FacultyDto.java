package com.example.demo.dto;

import com.example.demo.entity.Faculty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FacultyDto {
    private Long id;
    private String name;
    private Long universityId;

    public static FacultyDto toDto(Faculty faculty){
        return FacultyDto.builder()
                .id(faculty.getId())
                .name(faculty.getName().locale())
                .universityId(faculty.getUniversity().getId())
                .build();
    }
}
