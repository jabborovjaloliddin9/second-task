package com.example.demo.dto;

import com.example.demo.entity.BaseEntity;
import com.example.demo.entity.Journal;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JournalDto {
    private Long id;
    private String name;
    private Long groupId;
    private List<Long> subjectIds;

    public static JournalDto toDto(Journal journal) {
        return JournalDto.builder()
                .id(journal.getId())
                .name(journal.getName())
                .groupId(journal.getGroup().getId())
                .subjectIds(journal.getSubjects().stream().map(BaseEntity::getId).collect(Collectors.toList()))
                .build();
    }
}
