package com.example.demo.dto.form;

import com.example.demo.entity.Journal;
import com.example.demo.entity.Student;
import com.example.demo.entity.Subject;
import lombok.Getter;

import javax.persistence.ManyToOne;

@Getter
public class MarkForm {
    private Integer studentMark;
    private Long studentId;
    private Long subjectId;
}
