package com.example.demo.dto.form;

import lombok.Getter;

@Getter
public class GroupForm {
    private String name;
    private Long facultyId;
    private String year;
}
