package com.example.demo.dto.form;

import lombok.Getter;

import java.util.List;

@Getter
public class JournalRequest {
    private Long id;
    private List<Long> subjects;
}
