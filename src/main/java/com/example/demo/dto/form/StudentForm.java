package com.example.demo.dto.form;

import lombok.Getter;

@Getter
public class StudentForm {
    private String firstName;
    private String lastName;
    private Long groupId;
}
