package com.example.demo.dto.form;

import com.example.demo.entity.LocalizadString;
import lombok.Getter;

@Getter
public class FacultyForm {
    private LocalizadString name;
    private Long universityId;
}
