package com.example.demo.dto.form;

import com.example.demo.entity.LocalizadString;
import lombok.Getter;

@Getter
public class SubjectForm {
    private LocalizadString name;
}
