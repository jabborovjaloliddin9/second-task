package com.example.demo.dto.form;

import lombok.Getter;

import java.util.List;

@Getter
public class JournalForm {
    private String name;
    private Long groupId;
    private List<Long> subjectIds;
}
