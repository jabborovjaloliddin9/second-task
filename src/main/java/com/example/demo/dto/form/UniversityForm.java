package com.example.demo.dto.form;

import ch.qos.logback.core.joran.spi.NoAutoStart;
import com.example.demo.entity.LocalizadString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Date;

@Getter
public class UniversityForm {
    private LocalizadString name;
    private String address;
    private LocalDate open_year;
}
