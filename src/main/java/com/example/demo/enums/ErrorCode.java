package com.example.demo.enums;

import lombok.Getter;

@Getter
public enum ErrorCode {
    OBJECT_NOT_FOUND(1000),
    OBJECT_ALLREADY_EXISTS(1001),
    INVALID_MARK(1002),
    ;

    private int code;

    ErrorCode(int code) {
        this.code = code;
    }
}