package com.example.demo.repository;

import com.example.demo.dto.StudentNameResponse;
import com.example.demo.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long> {

    List<Student> findAllByDeletedFalse();

    Optional<Student> findByIdAndDeletedFalse(Long id);

    @Query(value = "select * from student where group_id = ?1 and deleted = false", nativeQuery = true)
    List<Student> findByGroup(Long groupId);

    @Query(value = "select count(id) from student where group_id = ?1 and deleted = false", nativeQuery = true)
    Integer getCountByGroup(Long groupId);

    @Query(value = "select s.id as id, s.first_name as firstName, s.last_name as lastName, g.id as groupId, " +
            "f.id as facultyId, u.id as universityId, avg(m.student_mark) as averageMark " +
            "from student s " +
            "full join groups g on s.group_id = g.id " +
            "full join faculty f on g.faculty_id = f.id " +
            "full join university u on f.university_id = u.id " +
            "left join mark m on s.id = m.student_id " +
            "where last_name = ?1 " +
            "and s.deleted = false " +
            "and g.deleted = false " +
            "and f.deleted = false " +
            "and u.deleted = false " +
            "group by s.id, s.first_name, s.last_name, g.id, f.id, u.id " +
            "order by avg(m.student_mark) desc", nativeQuery = true)
    List<StudentNameResponse> findByLastName(String lastName);

    @Query(value = "select s.id as id, s.first_name as firstName, s.last_name as lastName, g.id as groupId, " +
            "f.id as facultyId, u.id as universityId, avg(m.student_mark) as averageMark " +
            "from student s " +
            "full join groups g on s.group_id = g.id " +
            "full join faculty f on g.faculty_id = f.id " +
            "full join university u on f.university_id = u.id " +
            "left join mark m on s.id = m.student_id " +
            "where last_name = ?1 " +
            "and s.deleted = false " +
            "and g.deleted = false " +
            "and f.deleted = false " +
            "and u.deleted = false " +
            "group by s.id, s.first_name, s.last_name, g.id, f.id, u.id " +
            "order by avg(m.student_mark) desc", nativeQuery = true)
    List<StudentNameResponse> findByFirstName(String firstname);

    @Query(value = "select * " +
            "from student s " +
            "left join groups g on s.group_id = g.id " +
            "where faculty_id = ?1 " +
            "and s.deleted = false " +
            "and g.deleted = false", nativeQuery = true)
    List<Student> findByFaculty(Long facultyId);

    @Query(value = "select count(s.id) " +
            "from student s " +
            "left join groups g on s.group_id = g.id " +
            "where faculty_id = ?1 " +
            "and s.deleted = false " +
            "and g.deleted = false;", nativeQuery = true)
    Integer getCountByFaculty(Long facultyId);

    @Query(value = "select * " +
            "from student s " +
            "left join groups g on s.group_id = g.id " +
            "left join faculty f on g.faculty_id = f.id " +
            "where university_id = ?1 " +
            "and s.deleted = false " +
            "and g.deleted = false " +
            "and f.deleted = false", nativeQuery = true)
    List<Student> findByUniversity(Long universityId);

    @Query(value = "select count(s.id) " +
            "from student s " +
            "left join groups g on s.group_id = g.id " +
            "left join faculty f on g.faculty_id = f.id " +
            "where university_id = ?1 " +
            "and s.deleted = false " +
            "and g.deleted = false " +
            "and f.deleted = false", nativeQuery = true)
    Integer getCountByUniversity(Long universityId);

}
