package com.example.demo.repository;

import com.example.demo.entity.Faculty;
import com.example.demo.entity.Mark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface MarkRepository extends JpaRepository<Mark, Long> {

    List<Mark> findAllByDeletedFalse();

    Optional<Mark> findByIdAndDeletedFalse(Long id);

}
