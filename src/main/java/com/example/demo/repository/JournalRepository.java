package com.example.demo.repository;

import com.example.demo.entity.Group;
import com.example.demo.entity.Journal;
import com.example.demo.entity.Student;
import com.example.demo.entity.University;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface JournalRepository extends JpaRepository<Journal, Long> {

    List<Journal> findAllByDeletedFalse();

    Optional<Journal> findByIdAndDeletedFalse(Long id);

    Boolean existsByGroupAndDeletedFalse(Group group);

    Optional<Journal> findByGroupAndDeletedFalse(Group group);

    @Query(value = "select * " +
            "from student s " +
            "left join groups g on s.group_id = g.id " +
            "left join journal j on g.id = j.group_id " +
            "where s.id = ?1 " +
            "and s.deleted = false " +
            "and j.deleted = false " +
            "and g.deleted = false;", nativeQuery = true)
    Optional<Journal> getJournalByStudentId(Long studentId);
}
