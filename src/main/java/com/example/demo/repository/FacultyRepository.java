package com.example.demo.repository;

import com.example.demo.dto.StudentResponse;
import com.example.demo.entity.Faculty;
import com.example.demo.entity.Group;
import com.example.demo.entity.University;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface FacultyRepository extends JpaRepository<Faculty, Long> {

    List<Faculty> findAllByDeletedFalse();

    Optional<Faculty> findByIdAndDeletedFalse(Long id);

    @Query(value = "select * " +
            "from faculty f " +
            "left join university u on f.university_id = u.id " +
            "where university_id = ?1 " +
            "and f.deleted = false " +
            "and u.deleted = false;", nativeQuery = true)
    List<Faculty> findByUniversity(Long universityId);

    @Query(value = "select count(f.id) " +
            "from faculty f " +
            "left join university u on f.university_id = u.id " +
            "where university_id = ?1 " +
            "and f.deleted = false " +
            "and u.deleted = false;", nativeQuery = true)
    Integer getCountByUniversity(Long universityId);

    @Query(value = "select s.id as id, s.first_name as firstName, " +
            "s.last_name as lastName, avg(m.student_mark) as averageMark " +
            "from student s " +
            "left join groups g on s.group_id = g.id " +
            "left join faculty f on g.faculty_id = f.id " +
            "left join mark m on s.id = m.student_id " +
            "where f.id = ?1 " +
            "and s.deleted = false " +
            "and g.deleted = false " +
            "and f.deleted = false " +
            "group by s.id, s.first_name, s.last_name " +
            "order by avg(student_mark) desc", nativeQuery = true)
    List<StudentResponse> studentList(Long id);

}
