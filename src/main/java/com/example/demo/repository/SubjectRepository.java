package com.example.demo.repository;

import com.example.demo.entity.Faculty;
import com.example.demo.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface SubjectRepository extends JpaRepository<Subject, Long> {

    List<Subject> findAllByDeletedFalse();

    Optional<Subject> findByIdAndDeletedFalse(Long id);

    @Query(value = "select * from subject s where s.id In :ids", nativeQuery = true)
    List<Subject> findSubjects(List<Long> ids);

}
