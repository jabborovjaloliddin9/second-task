package com.example.demo.repository;

import com.example.demo.dto.StudentResponse;
import com.example.demo.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface GroupRepository extends JpaRepository<Group, Long> {

    List<Group> findAllByDeletedFalse();

    Optional<Group> findByIdAndDeletedFalse(Long id);

    @Query(value = "select * " +
            "from groups " +
            "where faculty_id = ?1 " +
            "and deleted = false", nativeQuery = true)
    List<Group> findByFaculty(Long facultyId);

    @Query(value = "select count(g.id) " +
            "from groups g " +
            "where faculty_id = 1 " +
            "and deleted = false", nativeQuery = true)
    Integer getCountByFaculty(Long facultyId);

    @Query(value = "select * " +
            "from groups g " +
            "left join faculty f on g.faculty_id = f.id " +
            "left join university u on f.university_id = u.id " +
            "where university_id = ?1 " +
            "and g.deleted = false " +
            "and f.deleted = false " +
            "and u.deleted = false", nativeQuery = true)
    List<Group> findByUniversity(Long universityId);

    @Query(value = "select count(g.id) " +
            "from groups g " +
            "left join faculty f on g.faculty_id = f.id " +
            "left join university u on f.university_id = u.id " +
            "where university_id = 2 " +
            "and f.deleted = false " +
            "and g.deleted = false " +
            "and u.deleted = false", nativeQuery = true)
    Integer getCountByUniversity(Long universityId);

    @Query(value = "select s.id as id, s.first_name as firstName, s.last_name as lastName, " +
            "avg(m.student_mark) as averageMark " +
            "from student s " +
            "left join groups g on s.group_id = g.id " +
            "left join mark m on s.id = m.student_id " +
            "where g.id = ?1 " +
            "and s.deleted = false " +
            "and g.deleted = false " +
            "group by s.id, s.first_name, s.last_name " +
            "order by avg(student_mark) desc", nativeQuery = true)
    List<StudentResponse> studentList(Long id);
}
