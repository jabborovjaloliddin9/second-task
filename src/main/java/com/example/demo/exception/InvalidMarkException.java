package com.example.demo.exception;

import com.example.demo.enums.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class InvalidMarkException extends UniversityException{
    private String objectName;

    @Override
    ErrorCode errorCode() {
        return ErrorCode.INVALID_MARK;
    }
}
