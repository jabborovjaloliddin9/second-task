package com.example.demo.exception;

import com.example.demo.enums.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ObjectNotFoundException extends UniversityException{
    private String objectName;
    private String id;

    @Override
    ErrorCode errorCode() {
        return ErrorCode.OBJECT_NOT_FOUND;
    }
}
