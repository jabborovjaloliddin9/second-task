package com.example.demo.exception;

import com.example.demo.enums.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ObjectAllreadyExistsException extends UniversityException{
    private String objectName;

    @Override
    ErrorCode errorCode() {
        return ErrorCode.OBJECT_ALLREADY_EXISTS;
    }
}
