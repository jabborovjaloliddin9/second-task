package com.example.demo.service;

import com.example.demo.dto.StudentResponse;
import com.example.demo.dto.UniversityDto;
import com.example.demo.dto.form.UniversityForm;

import java.util.List;

public interface UniversityService {

    UniversityDto getOne(Long id);

    List<UniversityDto> findAll();

    UniversityDto add(UniversityForm form);

    UniversityDto update(Long id, UniversityForm form);

    void delete(Long id);

    List<StudentResponse> studentList(Long id);
}
