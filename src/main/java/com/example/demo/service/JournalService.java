package com.example.demo.service;

import com.example.demo.dto.JournalDto;
import com.example.demo.dto.StudentDto;
import com.example.demo.dto.SubjectDto;
import com.example.demo.dto.form.JournalForm;
import com.example.demo.dto.form.JournalRequest;

import java.util.List;

public interface JournalService {

    JournalDto getOne(Long id);

    List<JournalDto> findAll();

    JournalDto add(JournalForm form);

    JournalDto update(Long id, JournalForm form);

    void delete(Long id);

    void addSubjects(JournalRequest request);

    void removeSubjects(JournalRequest request);

    List<StudentDto> findAllStudents(Long id);

    Integer getStudentCount(Long id);

}
