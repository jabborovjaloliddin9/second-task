package com.example.demo.service;

import com.example.demo.dto.StudentDto;
import com.example.demo.dto.StudentNameResponse;
import com.example.demo.dto.form.StudentForm;

import java.util.List;

public interface StudentService {

    StudentDto getOne(Long id);

    List<StudentDto> findAll();

    StudentDto add(StudentForm form);

    StudentDto update(Long id, StudentForm form);

    void delete(Long id);

    List<StudentNameResponse> getByFirstName(String firstname);

    List<StudentNameResponse> getByLastName(String lastname);

    List<StudentDto> findByGroup(Long groupId);

    Integer getCountByGroup(Long groupId);

    List<StudentDto> findByFaculty(Long facultyId);

    Integer getCountByFaculty(Long facultyId);

    List<StudentDto> findByUniversity(Long universityId);

    Integer getCountByUniversity(Long universityId);
}
