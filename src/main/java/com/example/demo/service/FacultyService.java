package com.example.demo.service;

import com.example.demo.dto.FacultyDto;
import com.example.demo.dto.StudentResponse;
import com.example.demo.dto.form.FacultyForm;

import java.util.List;

public interface FacultyService {

    FacultyDto getOne(Long id);

    List<FacultyDto> findAll();

    FacultyDto add(FacultyForm form);

    FacultyDto update(Long id, FacultyForm form);

    void delete(Long id);

    List<FacultyDto> findByUniversity(Long universityId);

    Integer getCountByUniversity(Long universityId);

    List<StudentResponse> studentList(Long id);
}
