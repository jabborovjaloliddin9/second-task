package com.example.demo.service;

import com.example.demo.dto.GroupDto;
import com.example.demo.dto.StudentDto;
import com.example.demo.dto.StudentResponse;
import com.example.demo.dto.form.GroupForm;

import java.util.List;

public interface GroupService {

    GroupDto getOne(Long id);

    List<GroupDto> findAll();

    GroupDto add(GroupForm form);

    GroupDto update(Long id, GroupForm form);

    void delete(Long id);

    List<GroupDto>  findByFaculty(Long facultyId);

    Integer getCountByFaculty(Long facultyId);

    List<GroupDto> findByUniversity(Long universityId);

    Integer getCountByUniversity(Long universityId);

    List<StudentResponse> studentList(Long id);

}
