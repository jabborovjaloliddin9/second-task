package com.example.demo.service;

import com.example.demo.dto.SubjectDto;
import com.example.demo.dto.form.SubjectForm;

import java.util.List;

public interface SubjectService {

    SubjectDto getOne(Long id);

    List<SubjectDto> findAll();

    SubjectDto add(SubjectForm form);

    SubjectDto update(Long id, SubjectForm form);

    void delete(Long id);

    List<SubjectDto> findByStudent(Long studentId);
}
