package com.example.demo.service.impl;

import com.example.demo.dto.StudentDto;
import com.example.demo.dto.StudentNameResponse;
import com.example.demo.dto.form.StudentForm;
import com.example.demo.entity.Student;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository repository;
    private final GroupServiceImpl groupService;
    private final FacultyServiceImpl facultyService;
    private final UniversityServiceImpl universityService;

    @Override
    public StudentDto getOne(Long id) {
        return StudentDto.toDto(get(id));
    }

    @Override
    public List<StudentDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(StudentDto::toDto).collect(Collectors.toList());
    }

    @Override
    public StudentDto add(StudentForm form) {
        return StudentDto.toDto(repository.save(fill(new Student(), form)));
    }

    @Override
    public StudentDto update(Long id, StudentForm form) {
        return StudentDto.toDto(repository.save(fill(get(id), form)));
    }

    @Override
    public void delete(Long id) {
        Student student = get(id);
        student.setDeleted(true);
        repository.save(student);
    }

    @Override
    public List<StudentNameResponse> getByFirstName(String firstname) {
        return repository.findByFirstName(firstname);
    }

    @Override
    public List<StudentNameResponse> getByLastName(String lastname) {
        return repository.findByLastName(lastname);
    }


    @Override
    public List<StudentDto> findByGroup(Long groupId) {
        return repository.findByGroup(groupId).stream().map(StudentDto::toDto).collect(Collectors.toList());
    }

    @Override
    public Integer getCountByGroup(Long groupId) {
        return repository.getCountByGroup(groupId);
    }

    @Override
    public List<StudentDto> findByFaculty(Long facultyId) {
        return repository.findByFaculty(facultyId).stream().map(StudentDto::toDto).collect(Collectors.toList());
    }

    @Override
    public Integer getCountByFaculty(Long facultyId) {
        return repository.getCountByFaculty(facultyId);
    }

    @Override
    public List<StudentDto> findByUniversity(Long universityId) {
        return repository.findByUniversity(universityId).stream().map(StudentDto::toDto).collect(Collectors.toList());
    }

    @Override
    public Integer getCountByUniversity(Long universityId) {
        return repository.getCountByUniversity(universityId);
    }

    public Student get(Long id) {
        Optional<Student> optional = repository.findByIdAndDeletedFalse(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new ObjectNotFoundException("Student", id.toString());
        }
    }

    private Student fill(Student student, StudentForm form){
        student.setFirstName(form.getFirstName());
        student.setLastName(form.getLastName());
        student.setGroup(groupService.get(form.getGroupId()));
        return student;
    }
}
