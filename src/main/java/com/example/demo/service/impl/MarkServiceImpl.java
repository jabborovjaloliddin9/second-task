package com.example.demo.service.impl;

import com.example.demo.dto.MarkDto;
import com.example.demo.dto.form.MarkForm;
import com.example.demo.entity.Journal;
import com.example.demo.entity.Mark;
import com.example.demo.entity.Student;
import com.example.demo.entity.Subject;
import com.example.demo.exception.InvalidMarkException;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.MarkRepository;
import com.example.demo.service.MarkService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MarkServiceImpl implements MarkService {

    private final MarkRepository repository;
    private final JournalServiceImpl journalService;
    private final StudentServiceImpl studentService;
    private final SubjectServiceImpl subjectService;

    @Override
    public MarkDto getOne(Long id) {
        return MarkDto.toDto(get(id));
    }

    @Override
    public List<MarkDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(MarkDto::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public MarkDto add(MarkForm form) {
        return MarkDto.toDto(repository.save(fill(new Mark(), form)));
    }

    @Override
    @Transactional
    public MarkDto update(Long id, MarkForm form) {
        return MarkDto.toDto(repository.save(fill(get(id), form)));
    }

    @Override
    public void delete(Long id) {
        Mark mark = get(id);
        mark.setDeleted(true);
        repository.save(mark);
    }

    public Mark get(Long id) {
        Optional<Mark> optional = repository.findByIdAndDeletedFalse(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new ObjectNotFoundException("Mark", id.toString());
        }
    }

    private Mark fill(Mark mark, MarkForm form){
        if (form.getStudentMark() > 100 || form.getStudentMark() < 0) {
            throw new InvalidMarkException(form.getStudentMark().toString());
        }
        mark.setStudentMark(form.getStudentMark());
        mark.setStudent(studentService.get(form.getStudentId()));
        mark.setJournal(journalService.getByStudentId(form.getStudentId()));
        mark.setSubject(subjectService.get(form.getSubjectId()));
        return mark;
    }
}
