package com.example.demo.service.impl;

import com.example.demo.dto.StudentResponse;
import com.example.demo.dto.UniversityDto;
import com.example.demo.dto.form.UniversityForm;
import com.example.demo.entity.University;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.UniversityRepository;
import com.example.demo.service.UniversityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UniversityServiceImpl implements UniversityService {

    private final UniversityRepository repository;

    @Override
    public UniversityDto getOne(Long id) {
        return UniversityDto.toDto(get(id));
    }

    @Override
    public List<UniversityDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(UniversityDto::toDto).collect(Collectors.toList());
    }

    @Override
    public UniversityDto add(UniversityForm form) {
        return UniversityDto.toDto(repository.save(fill(new University(), form)));
    }

    @Override
    public UniversityDto update(Long id, UniversityForm form) {
        return UniversityDto.toDto(repository.save(fill(get(id), form)));
    }

    @Override
    public void delete(Long id) {
        University university = get(id);
        university.setDeleted(true);
        repository.save(university);
    }

    @Override
    public List<StudentResponse> studentList(Long id) {
        return repository.studentList(id);
    }

    public University get(Long id) {
        Optional<University> optional = repository.findByIdAndDeletedFalse(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new ObjectNotFoundException("University", id.toString());
        }
    }

    private University fill(University university, UniversityForm form){
        university.setName(form.getName());
        university.setAddress(form.getAddress());
        university.setOpen_year(form.getOpen_year());
        return university;
    }
}
