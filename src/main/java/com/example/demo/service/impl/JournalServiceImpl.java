package com.example.demo.service.impl;

import com.example.demo.dto.JournalDto;
import com.example.demo.dto.StudentDto;
import com.example.demo.dto.form.JournalForm;
import com.example.demo.dto.form.JournalRequest;
import com.example.demo.entity.Group;
import com.example.demo.entity.Journal;
import com.example.demo.entity.Student;
import com.example.demo.entity.Subject;
import com.example.demo.exception.ObjectAllreadyExistsException;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.JournalRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.repository.SubjectRepository;
import com.example.demo.service.JournalService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JournalServiceImpl implements JournalService {

    private final JournalRepository repository;
    private final GroupServiceImpl groupService;
    private final SubjectRepository subjectRepository;
    private final StudentRepository studentRepository;

    @Override
    public JournalDto getOne(Long id) {
        return JournalDto.toDto(get(id));
    }

    @Override
    public List<JournalDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(JournalDto::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public JournalDto add(JournalForm form) {
        return JournalDto.toDto(repository.save(fill(new Journal(), form)));
    }

    @Override
    public JournalDto update(Long id, JournalForm form) {
        return JournalDto.toDto(repository.save(fill(get(id), form)));
    }

    @Override
    public void delete(Long id) {
        Journal journal = get(id);
        journal.setDeleted(true);
        repository.save(journal);
    }

    @Override
    @Transactional
    public void addSubjects(JournalRequest request) {
        List<Subject> subjects = subjectRepository.findSubjects(request.getSubjects());
        Journal journal = get(request.getId());
        List<Subject> journalSubjects = journal.getSubjects();
        subjects.forEach(it -> {
            if (!journalSubjects.contains(it)) journalSubjects.add(it);
        });
        journal.setSubjects(journalSubjects);
        repository.save(journal);
    }

    @Override
    @Transactional
    public void removeSubjects(JournalRequest request) {
        List<Subject> subjects = subjectRepository.findSubjects(request.getSubjects());
        Journal journal = get(request.getId());
        journal.getSubjects().removeAll(subjects);
        repository.save(journal);
    }

    @Override
    public List<StudentDto> findAllStudents(Long id) {
        return studentRepository.findByGroup(get(id).getGroup().getId())
                .stream().map(StudentDto::toDto).collect(Collectors.toList());
    }

    @Override
    public Integer getStudentCount(Long id) {
        return studentRepository.getCountByGroup(get(id).getGroup().getId());
    }

    public Journal get(Long id) {
        Optional<Journal> optional = repository.findByIdAndDeletedFalse(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new ObjectNotFoundException("Journal", id.toString());
        }
    }

    public Journal getByStudentId(Long studentId){
        Optional<Journal> optional = repository.getJournalByStudentId(studentId);
        if (optional.isPresent()){
            return optional.get();
        } else {
            throw new ObjectNotFoundException("Journal", studentId.toString());
        }
    }

    private Journal fill(Journal journal, JournalForm form) {
        Group group = groupService.get(form.getGroupId());
        if (repository.existsByGroupAndDeletedFalse(group)) {
            throw new ObjectAllreadyExistsException("Journal");
        }
        journal.setName(form.getName());
        journal.setGroup(group);
        journal.setSubjects(subjectRepository.findSubjects(form.getSubjectIds()));
        return journal;
    }
}
