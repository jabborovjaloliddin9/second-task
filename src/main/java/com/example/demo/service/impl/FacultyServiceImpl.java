package com.example.demo.service.impl;

import com.example.demo.dto.FacultyDto;
import com.example.demo.dto.StudentResponse;
import com.example.demo.dto.form.FacultyForm;
import com.example.demo.entity.Faculty;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.FacultyRepository;
import com.example.demo.service.FacultyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FacultyServiceImpl implements FacultyService {

    private final FacultyRepository repository;
    private final UniversityServiceImpl universityService;

    @Override
    public FacultyDto getOne(Long id) {
        return FacultyDto.toDto(get(id));
    }

    @Override
    public List<FacultyDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(FacultyDto::toDto).collect(Collectors.toList());
    }

    @Override
    public FacultyDto add(FacultyForm form) {
        return FacultyDto.toDto(repository.save(fill(new Faculty(), form)));
    }

    @Override
    public FacultyDto update(Long id, FacultyForm form) {
        return FacultyDto.toDto(repository.save(fill(get(id), form)));
    }

    @Override
    public void delete(Long id) {
        Faculty faculty = get(id);
        faculty.setDeleted(true);
        repository.save(faculty);
    }

    @Override
    public List<FacultyDto> findByUniversity(Long universityId) {
        return repository.findByUniversity(universityId).stream().map(FacultyDto::toDto).collect(Collectors.toList());
    }

    @Override
    public Integer getCountByUniversity(Long universityId) {
        return repository.getCountByUniversity(universityId);
    }

    @Override
    public List<StudentResponse> studentList(Long id) {
        return repository.studentList(id);
    }

    public Faculty get(Long id) {
        Optional<Faculty> optional = repository.findByIdAndDeletedFalse(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new ObjectNotFoundException("Faculty", id.toString());
        }
    }

    private Faculty fill(Faculty faculty, FacultyForm form){
        faculty.setName(form.getName());
        faculty.setUniversity(universityService.get(form.getUniversityId()));
        return faculty;
    }
}
