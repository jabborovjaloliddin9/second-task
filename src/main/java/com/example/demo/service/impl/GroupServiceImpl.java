package com.example.demo.service.impl;

import com.example.demo.dto.GroupDto;
import com.example.demo.dto.StudentResponse;
import com.example.demo.dto.form.GroupForm;
import com.example.demo.entity.Group;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.GroupRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository repository;
    private final FacultyServiceImpl facultyService;
    private final StudentRepository studentRepository;

    @Override
    public GroupDto getOne(Long id) {
        return GroupDto.toDto(get(id));
    }

    @Override
    public List<GroupDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(GroupDto::toDto).collect(Collectors.toList());
    }

    @Override
    public GroupDto add(GroupForm form) {
        return GroupDto.toDto(repository.save(fill(new Group(), form)));
    }

    @Override
    public GroupDto update(Long id, GroupForm form) {
        return GroupDto.toDto(repository.save(fill(get(id), form)));
    }

    @Override
    public void delete(Long id) {
        Group group = get(id);
        group.setDeleted(true);
        repository.save(group);
    }

    @Override
    public List<GroupDto> findByFaculty(Long facultyId) {
        return repository.findByFaculty(facultyId).stream().map(GroupDto::toDto).collect(Collectors.toList());
    }

    @Override
    public Integer getCountByFaculty(Long facultyId) {
        return repository.getCountByFaculty(facultyId);
    }

    @Override
    public List<GroupDto> findByUniversity(Long universityId) {
        return repository.findByIdAndDeletedFalse(universityId).stream().map(GroupDto::toDto).collect(Collectors.toList());
    }

    @Override
    public Integer getCountByUniversity(Long universityId) {
        return repository.getCountByUniversity(universityId);
    }

    @Override
    public List<StudentResponse> studentList(Long id) {
        return repository.studentList(id);
    }

    public Group get(Long id) {
        Optional<Group> optional = repository.findByIdAndDeletedFalse(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new ObjectNotFoundException("Group", id.toString());
        }
    }

    private Group fill(Group group, GroupForm form){
        group.setName(form.getName());
        group.setFaculty(facultyService.get(form.getFacultyId()));
        group.setYear(form.getYear());
        return group;
    }
}
