package com.example.demo.service.impl;

import com.example.demo.dto.SubjectDto;
import com.example.demo.dto.form.SubjectForm;
import com.example.demo.entity.Group;
import com.example.demo.entity.Journal;
import com.example.demo.entity.Student;
import com.example.demo.entity.Subject;
import com.example.demo.exception.ObjectNotFoundException;
import com.example.demo.repository.JournalRepository;
import com.example.demo.repository.SubjectRepository;
import com.example.demo.service.SubjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepository repository;
    private final StudentServiceImpl studentService;
    private final JournalRepository journalRepository;

    @Override
    public SubjectDto getOne(Long id) {
        return SubjectDto.toDto(get(id));
    }

    @Override
    public List<SubjectDto> findAll() {
        return repository.findAllByDeletedFalse().stream().map(SubjectDto::toDto).collect(Collectors.toList());
    }

    @Override
    public SubjectDto add(SubjectForm form) {
        return SubjectDto.toDto(repository.save(fill(new Subject(), form)));
    }

    @Override
    public SubjectDto update(Long id, SubjectForm form) {
        return SubjectDto.toDto(repository.save(fill(get(id), form)));
    }

    @Override
    public void delete(Long id) {
        Subject subject = get(id);
        subject.setDeleted(true);
        repository.save(subject);
    }

    @Override
    public List<SubjectDto> findByStudent(Long studentId) {
        Optional<Journal> optional =
                journalRepository.findByGroupAndDeletedFalse(studentService.get(studentId).getGroup());
        if (optional.isEmpty()) {
            throw new ObjectNotFoundException("Journal", studentId.toString());
        }
        return optional.get().getSubjects().stream().map(SubjectDto::toDto).collect(Collectors.toList());
    }

    public Subject get(Long id) {
        Optional<Subject> optional = repository.findByIdAndDeletedFalse(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new ObjectNotFoundException("Subject", id.toString());
        }
    }

    private Subject fill(Subject subject, SubjectForm form){
        subject.setName(form.getName());
        return subject;
    }
}
