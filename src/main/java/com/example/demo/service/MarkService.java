package com.example.demo.service;

import com.example.demo.dto.MarkDto;
import com.example.demo.dto.form.MarkForm;

import java.util.List;

public interface MarkService {

    MarkDto getOne(Long id);

    List<MarkDto> findAll();

    MarkDto add(MarkForm form);

    MarkDto update(Long id, MarkForm form);

    void delete(Long id);

}
